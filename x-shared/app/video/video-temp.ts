import { Video } from '../video/video-model';

export const Videos: Video[] = [
    {name: 'Video1', url: 'https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4'},
    {name: 'Video2', url: 'https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4'},
    {name: 'Video3', url: 'https://test.com/'},
    {name: 'Video4', url: 'https://test.com/'},
    {name: 'Video5', url: 'https://test.com/'}
];