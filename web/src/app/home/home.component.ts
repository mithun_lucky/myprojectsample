import { Component, OnInit } from '@angular/core';
import { Router, RouterModule, Routes } from '@angular/router';
import {FormGroup, FormBuilder, Validators } from '@angular/forms';  //Form Validation
import { HomeCommonViewModel } from '../../x-shared/app/home/home-common.view-model';
import { UserService } from '../../x-shared/app/signin/signin-common.service';   //Consuming the service

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [HomeCommonViewModel]
})
export class HomeComponent implements OnInit{

form : FormGroup;

constructor(private fb: FormBuilder) { }

  ngOnInit () {
    this.form = this.fb.group({
      'username': [null, Validators.required],
      'password': [null, Validators.required, Validators.minLength(6), Validators.maxLength(16)],
      
    });

    //constructor(public cvm: HomeCommonViewModel) {

  }


}