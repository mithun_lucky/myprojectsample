import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import {RouterModule, Route, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';
import { RegistrationComponent } from './registration/registration.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    RegistrationComponent,
    ForgotPasswordComponent
  ],
  imports: [
    RouterModule.forRoot([
      {path: '', redirectTo: 'login', pathMatch: 'full'},
    {path: 'login', component: HomeComponent},
    {path: 'registration', component: RegistrationComponent},
    {path: 'forgot-password', component: ForgotPasswordComponent},
    {path: '**', redirectTo: 'registration'}
    ]),
    BrowserModule,
    HttpModule,
    AppRoutingModule,
    SharedModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
