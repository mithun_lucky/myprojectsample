import { Injectable } from '@angular/core';

@Injectable()
export class SessionData {
    token: string = "";
    constructor() {}

    clearSessionData() {
        this.token = "";
    }
}