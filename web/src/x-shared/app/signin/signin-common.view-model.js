"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var EmailValidator = require("email-validator");
var User = (function () {
    function User() {
    }
    User.prototype.isValidEmail = function () {
        return EmailValidator.validate(this.email);
    };
    return User;
}());
exports.User = User;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2lnbmluLWNvbW1vbi52aWV3LW1vZGVsLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsic2lnbmluLWNvbW1vbi52aWV3LW1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsZ0RBQWtEO0FBRWxEO0lBQUE7SUFRQSxDQUFDO0lBSEMsMkJBQVksR0FBWjtRQUNFLE1BQU0sQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUM3QyxDQUFDO0lBQ0gsV0FBQztBQUFELENBQUMsQUFSRCxJQVFDO0FBUlksb0JBQUkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBFbWFpbFZhbGlkYXRvciBmcm9tICdlbWFpbC12YWxpZGF0b3InO1xuXG5leHBvcnQgY2xhc3MgVXNlciB7XG4gIGVtYWlsOiBzdHJpbmc7XG4gIHVuYW1lOiBzdHJpbmc7XG4gIHBhc3N3b3JkOiBzdHJpbmc7XG5cbiAgaXNWYWxpZEVtYWlsKCkge1xuICAgIHJldHVybiBFbWFpbFZhbGlkYXRvci52YWxpZGF0ZSh0aGlzLmVtYWlsKTtcbiAgfVxufSJdfQ==