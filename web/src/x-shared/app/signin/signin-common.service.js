"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
require("rxjs/add/operator/do");
require("rxjs/add/operator/map");
var adapter_1 = require("../../providers/adapter");
var methodMapping_1 = require("../../providers/methodMapping");
var sessionData_1 = require("../../providers/sessionData");
var UserService = (function () {
    function UserService(adapter, sessionData) {
        this.adapter = adapter;
        this.sessionData = sessionData;
    }
    UserService.prototype.register = function (user) {
        //call adapter.ts to register user
    };
    UserService.prototype.login = function (user) {
        var _this = this;
        var data = JSON.stringify({
            username: user.uname,
            password: user.password
        });
        return this.adapter.postRequest(data, methodMapping_1.MethodMapping.LOGIN)
            .map(function (response) {
            var tokenValue = response.headers.get("Authorization");
            _this.sessionData.token = tokenValue;
        });
    };
    UserService.prototype.getUserData = function () {
        return this.adapter.getRequest(methodMapping_1.MethodMapping.USERS, this.sessionData.token)
            .map(function (response) {
            //save response in session 
        });
    };
    UserService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [adapter_1.Adapter,
            sessionData_1.SessionData])
    ], UserService);
    return UserService;
}());
exports.UserService = UserService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2lnbmluLWNvbW1vbi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsic2lnbmluLWNvbW1vbi5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTJDO0FBRzNDLGdDQUE4QjtBQUM5QixpQ0FBK0I7QUFHL0IsbURBQWtEO0FBQ2xELCtEQUE4RDtBQUM5RCwyREFBMEQ7QUFHMUQ7SUFDRSxxQkFBb0IsT0FBZ0IsRUFDMUIsV0FBd0I7UUFEZCxZQUFPLEdBQVAsT0FBTyxDQUFTO1FBQzFCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO0lBQUcsQ0FBQztJQUV0Qyw4QkFBUSxHQUFSLFVBQVMsSUFBVTtRQUNqQixrQ0FBa0M7SUFDcEMsQ0FBQztJQUVELDJCQUFLLEdBQUwsVUFBTSxJQUFVO1FBQWhCLGlCQVdDO1FBVkMsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztZQUN4QixRQUFRLEVBQUUsSUFBSSxDQUFDLEtBQUs7WUFDcEIsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO1NBQ3hCLENBQUMsQ0FBQTtRQUVGLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsNkJBQWEsQ0FBQyxLQUFLLENBQUM7YUFDekQsR0FBRyxDQUFDLFVBQUEsUUFBUTtZQUNULElBQUksVUFBVSxHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQ3ZELEtBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxHQUFHLFVBQVUsQ0FBQztRQUN4QyxDQUFDLENBQUMsQ0FBQTtJQUNKLENBQUM7SUFFRCxpQ0FBVyxHQUFYO1FBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLDZCQUFhLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO2FBQzFFLEdBQUcsQ0FBQyxVQUFBLFFBQVE7WUFDWCwyQkFBMkI7UUFDL0IsQ0FBQyxDQUFDLENBQUE7SUFDRixDQUFDO0lBMUJVLFdBQVc7UUFEdkIsaUJBQVUsRUFBRTt5Q0FFa0IsaUJBQU87WUFDYix5QkFBVztPQUZ2QixXQUFXLENBMkJ2QjtJQUFELGtCQUFDO0NBQUEsQUEzQkQsSUEyQkM7QUEzQlksa0NBQVciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tIFwicnhqcy9SeFwiO1xuXG5pbXBvcnQgXCJyeGpzL2FkZC9vcGVyYXRvci9kb1wiO1xuaW1wb3J0IFwicnhqcy9hZGQvb3BlcmF0b3IvbWFwXCI7XG5cbmltcG9ydCB7IFVzZXIgfSBmcm9tIFwiLi9zaWduaW4tY29tbW9uLnZpZXctbW9kZWxcIjtcbmltcG9ydCB7IEFkYXB0ZXIgfSBmcm9tIFwiLi4vLi4vcHJvdmlkZXJzL2FkYXB0ZXJcIjtcbmltcG9ydCB7IE1ldGhvZE1hcHBpbmcgfSBmcm9tIFwiLi4vLi4vcHJvdmlkZXJzL21ldGhvZE1hcHBpbmdcIjtcbmltcG9ydCB7IFNlc3Npb25EYXRhIH0gZnJvbSBcIi4uLy4uL3Byb3ZpZGVycy9zZXNzaW9uRGF0YVwiO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgVXNlclNlcnZpY2Uge1xuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGFkYXB0ZXI6IEFkYXB0ZXIsIFxuICAgIHByaXZhdGUgc2Vzc2lvbkRhdGE6IFNlc3Npb25EYXRhKSB7fVxuXG4gIHJlZ2lzdGVyKHVzZXI6IFVzZXIpIHtcbiAgICAvL2NhbGwgYWRhcHRlci50cyB0byByZWdpc3RlciB1c2VyXG4gIH1cblxuICBsb2dpbih1c2VyOiBVc2VyKSB7XG4gICAgdmFyIGRhdGEgPSBKU09OLnN0cmluZ2lmeSh7XG4gICAgICB1c2VybmFtZTogdXNlci51bmFtZSxcbiAgICAgIHBhc3N3b3JkOiB1c2VyLnBhc3N3b3JkXG4gICAgfSlcblxuICAgIHJldHVybiB0aGlzLmFkYXB0ZXIucG9zdFJlcXVlc3QoZGF0YSwgTWV0aG9kTWFwcGluZy5MT0dJTilcbiAgICAubWFwKHJlc3BvbnNlID0+IHtcbiAgICAgICAgdmFyIHRva2VuVmFsdWUgPSByZXNwb25zZS5oZWFkZXJzLmdldChcIkF1dGhvcml6YXRpb25cIik7XG4gICAgICAgIHRoaXMuc2Vzc2lvbkRhdGEudG9rZW4gPSB0b2tlblZhbHVlO1xuICAgIH0pXG4gIH1cblxuICBnZXRVc2VyRGF0YSgpIHtcbiAgICByZXR1cm4gdGhpcy5hZGFwdGVyLmdldFJlcXVlc3QoTWV0aG9kTWFwcGluZy5VU0VSUywgdGhpcy5zZXNzaW9uRGF0YS50b2tlbilcbiAgICAubWFwKHJlc3BvbnNlID0+IHtcbiAgICAgIC8vc2F2ZSByZXNwb25zZSBpbiBzZXNzaW9uIFxuICB9KVxuICB9XG59Il19