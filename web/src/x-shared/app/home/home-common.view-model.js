"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var HomeCommonViewModel = (function () {
    function HomeCommonViewModel() {
        this.counter = 16;
    }
    Object.defineProperty(HomeCommonViewModel.prototype, "message", {
        get: function () {
            if (this.counter > 0) {
                return this.counter + ' taps left';
            }
            else {
                return 'Hoorraaay! \nYou are ready to start building!';
            }
        },
        enumerable: true,
        configurable: true
    });
    HomeCommonViewModel.prototype.onTap = function () {
        this.counter--;
    };
    HomeCommonViewModel = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], HomeCommonViewModel);
    return HomeCommonViewModel;
}());
exports.HomeCommonViewModel = HomeCommonViewModel;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9tZS1jb21tb24udmlldy1tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImhvbWUtY29tbW9uLnZpZXctbW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkM7QUFHM0M7SUFJRTtRQUZBLFlBQU8sR0FBVyxFQUFFLENBQUM7SUFFTCxDQUFDO0lBRWpCLHNCQUFJLHdDQUFPO2FBQVg7WUFDRSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3JCLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLFlBQVksQ0FBQztZQUNyQyxDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ04sTUFBTSxDQUFDLCtDQUErQyxDQUFDO1lBQ3pELENBQUM7UUFDSCxDQUFDOzs7T0FBQTtJQUVELG1DQUFLLEdBQUw7UUFDRSxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDakIsQ0FBQztJQWhCVSxtQkFBbUI7UUFEL0IsaUJBQVUsRUFBRTs7T0FDQSxtQkFBbUIsQ0FpQi9CO0lBQUQsMEJBQUM7Q0FBQSxBQWpCRCxJQWlCQztBQWpCWSxrREFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBIb21lQ29tbW9uVmlld01vZGVsIHtcblxuICBjb3VudGVyOiBudW1iZXIgPSAxNjtcblxuICBjb25zdHJ1Y3RvcigpIHsgfVxuXG4gIGdldCBtZXNzYWdlKCk6IHN0cmluZyB7XG4gICAgaWYgKHRoaXMuY291bnRlciA+IDApIHtcbiAgICAgIHJldHVybiB0aGlzLmNvdW50ZXIgKyAnIHRhcHMgbGVmdCc7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiAnSG9vcnJhYWF5ISBcXG5Zb3UgYXJlIHJlYWR5IHRvIHN0YXJ0IGJ1aWxkaW5nISc7XG4gICAgfVxuICB9XG5cbiAgb25UYXAoKSB7XG4gICAgdGhpcy5jb3VudGVyLS07XG4gIH1cbn1cbiJdfQ==