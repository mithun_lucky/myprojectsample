import { Component, OnInit, ChangeDetectorRef } from '@angular/core';import { User } from "../../x-shared/app/signin/signin-common.view-model";

import { Page } from "ui/page";
import { Button } from "ui/button";
import { screen } from "tns-core-modules/platform";
import { Item } from "../../x-shared/providers/slideItem/item";
import { ItemService } from "../../x-shared/providers/slideItem/item.service";

import * as appSettings from "application-settings";
import { UserService } from "../../x-shared/app/signin/signin-common.service";
import { UtilityComponent } from "../utils/utilities";

@Component({
  selector: "signin",
  providers: [UserService,ItemService],
  templateUrl: "modules/signin/signin.component.html",
  styleUrls: ["modules/signin/signin.component.scss"]
})

export class SigninComponent implements OnInit {
  user: User;
  isLoggingIn = true;
  password: string;

  showIntro = false;
  items: Item[];
  bottomContainer: Button;   

  constructor(private userService: UserService, private page: Page, private itemService: ItemService,
   private cdr: ChangeDetectorRef, private utilityHandler: UtilityComponent) {
    this.user = new User();
  }

  ngAfterViewInit(){
     var boolValue = appSettings.getBoolean("appFirstRun", false);
     if(!boolValue){
         this.items = this.itemService.getItems();
         this.showIntro = true;
    }  
   this.cdr.detectChanges();
 }

 showLogin(){     
    appSettings.setBoolean("appFirstRun", true);
    this.showIntro = false;
 }

  ngOnInit() {
    this.page.actionBarHidden = true;
  }

  submit() {
    if (this.isLoggingIn) {
      this.signIn();
    } else {
      if (this.user.isValidEmail()) {
        this.signUp();
      } else {
        alert("Please enter a valid email address.");
      }
    }
  }

  signIn() {
    // *****************Hardcoded version
    if (this.user.uname == "liquidhub" && this.user.password == "liquidhub") {
      this.utilityHandler.forwardNavigation("/home", true);
    } else {
      alert("Unfortunately we could not find your account.");
    }

    // ******************Get the data from backend
  //  this.userService.login(this.user)
  //  .subscribe( response => {
  //       this.getUserData();
  //     },
  //   (error) => {
  //     alert("Unfortunately we could not find your account.");
  //   });
  // } 

  // getUserData() {
  //   this.userService.getUserData()
  //   .subscribe( response => {
  //   this.utilityHandler.forwardNavigation("/home", true);
  //      },
  //    (error) => {
  //      alert("Unfortunately there is some issue. Please try again after some time.");
  //    });
 
  }
  
  signUp() {
    alert("We will get back to you soon!!");
  }

  toggleDisplay() {
    this.isLoggingIn = !this.isLoggingIn;
  }
}