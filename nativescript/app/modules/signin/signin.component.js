"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var signin_common_view_model_1 = require("../../x-shared/app/signin/signin-common.view-model");
var signin_common_service_1 = require("../../x-shared/app/signin/signin-common.service");
var page_1 = require("ui/page");
require("rxjs/add/operator/do");
var item_service_1 = require("../../x-shared/providers/slideItem/item.service");
var router_1 = require("nativescript-angular/router");
var appSettings = require("application-settings");
var SigninComponent = (function () {
    function SigninComponent(userService, page, itemService, cdr, routerExtensions) {
        this.userService = userService;
        this.page = page;
        this.itemService = itemService;
        this.cdr = cdr;
        this.routerExtensions = routerExtensions;
        this.isLoggingIn = true;
        this.showIntro = false;
        this.user = new signin_common_view_model_1.User();
    }
    SigninComponent.prototype.ngAfterViewInit = function () {
        var boolValue = appSettings.getBoolean("appFirstRun", false);
        if (!boolValue) {
            this.items = this.itemService.getItems();
            this.showIntro = true;
        }
        this.cdr.detectChanges();
    };
    SigninComponent.prototype.showLogin = function () {
        appSettings.setBoolean("appFirstRun", true);
        this.showIntro = false;
    };
    SigninComponent.prototype.ngOnInit = function () {
        this.page.actionBarHidden = true;
    };
    SigninComponent.prototype.submit = function () {
        if (this.isLoggingIn) {
            this.signIn();
        }
        else {
            if (this.user.isValidEmail()) {
                this.signUp();
            }
            else {
                alert("Please enter a valid email address.");
            }
        }
    };
    SigninComponent.prototype.signIn = function () {
        // *****************Hardcoded version
        // if (this.user.uname == "liquidhub" && this.user.password == "liquidhub") {
        //   this.routerExtensions.navigate(["/home"], {
        //     clearHistory: true,
        //     transition: {
        //         name: "slide",
        //         curve: "linear"
        //     }
        // });
        // } else {
        //   alert("Unfortunately we could not find your account.");
        // }
        var _this = this;
        // ******************Get the data from backend
        this.userService.login(this.user)
            .subscribe(function (response) {
            _this.getUserData();
        }, function (error) {
            alert("Unfortunately we could not find your account.");
        });
    };
    SigninComponent.prototype.getUserData = function () {
        var _this = this;
        this.userService.getUserData()
            .subscribe(function (response) {
            _this.routerExtensions.navigate(["/home"], {
                clearHistory: true,
                transition: {
                    name: "slide",
                    curve: "linear"
                }
            });
        }, function (error) {
            alert("Unfortunately there is some issue. Please try again after some time.");
        });
    };
    SigninComponent.prototype.signUp = function () {
        alert("We will get back to you soon!!");
    };
    SigninComponent.prototype.toggleDisplay = function () {
        this.isLoggingIn = !this.isLoggingIn;
    };
    SigninComponent = __decorate([
        core_1.Component({
            selector: "signin",
            providers: [signin_common_service_1.UserService, item_service_1.ItemService],
            templateUrl: "modules/signin/signin.component.html",
            styleUrls: ["modules/signin/signin.component.scss"]
        }),
        __metadata("design:paramtypes", [signin_common_service_1.UserService, page_1.Page, item_service_1.ItemService,
            core_1.ChangeDetectorRef, router_1.RouterExtensions])
    ], SigninComponent);
    return SigninComponent;
}());
exports.SigninComponent = SigninComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2lnbmluLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNpZ25pbi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkY7QUFBQSwrRkFBMEU7QUFDcksseUZBQThFO0FBQzlFLGdDQUErQjtBQUUvQixnQ0FBOEI7QUFLOUIsZ0ZBQThFO0FBQzlFLHNEQUE2RDtBQUU3RCxrREFBb0Q7QUFTcEQ7SUFTRSx5QkFBb0IsV0FBd0IsRUFBVSxJQUFVLEVBQVUsV0FBd0IsRUFDekYsR0FBc0IsRUFBVSxnQkFBa0M7UUFEdkQsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFBVSxTQUFJLEdBQUosSUFBSSxDQUFNO1FBQVUsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDekYsUUFBRyxHQUFILEdBQUcsQ0FBbUI7UUFBVSxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBUjNFLGdCQUFXLEdBQUcsSUFBSSxDQUFDO1FBR25CLGNBQVMsR0FBRyxLQUFLLENBQUM7UUFNaEIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLCtCQUFJLEVBQUUsQ0FBQztJQUN6QixDQUFDO0lBRUQseUNBQWUsR0FBZjtRQUNHLElBQUksU0FBUyxHQUFHLFdBQVcsQ0FBQyxVQUFVLENBQUMsYUFBYSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQzdELEVBQUUsQ0FBQSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUEsQ0FBQztZQUNYLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN6QyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUMzQixDQUFDO1FBQ0YsSUFBSSxDQUFDLEdBQUcsQ0FBQyxhQUFhLEVBQUUsQ0FBQztJQUMzQixDQUFDO0lBRUQsbUNBQVMsR0FBVDtRQUNHLFdBQVcsQ0FBQyxVQUFVLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzVDLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO0lBQzFCLENBQUM7SUFFQSxrQ0FBUSxHQUFSO1FBQ0UsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO0lBQ25DLENBQUM7SUFFRCxnQ0FBTSxHQUFOO1FBQ0UsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7WUFDckIsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ2hCLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUM3QixJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7WUFDaEIsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNOLEtBQUssQ0FBQyxxQ0FBcUMsQ0FBQyxDQUFDO1lBQy9DLENBQUM7UUFDSCxDQUFDO0lBQ0gsQ0FBQztJQUVELGdDQUFNLEdBQU47UUFDRSxxQ0FBcUM7UUFDckMsNkVBQTZFO1FBQzdFLGdEQUFnRDtRQUNoRCwwQkFBMEI7UUFDMUIsb0JBQW9CO1FBQ3BCLHlCQUF5QjtRQUN6QiwwQkFBMEI7UUFDMUIsUUFBUTtRQUNSLE1BQU07UUFDTixXQUFXO1FBQ1gsNERBQTREO1FBQzVELElBQUk7UUFaTixpQkFzQkM7UUFSQyw4Q0FBOEM7UUFDL0MsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQzthQUNoQyxTQUFTLENBQUUsVUFBQSxRQUFRO1lBQ2YsS0FBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3JCLENBQUMsRUFDSCxVQUFDLEtBQUs7WUFDSixLQUFLLENBQUMsK0NBQStDLENBQUMsQ0FBQztRQUN6RCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxxQ0FBVyxHQUFYO1FBQUEsaUJBZUM7UUFkQyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRTthQUM3QixTQUFTLENBQUUsVUFBQSxRQUFRO1lBQ2hCLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxPQUFPLENBQUMsRUFBRTtnQkFDMUMsWUFBWSxFQUFFLElBQUk7Z0JBQ2xCLFVBQVUsRUFBRTtvQkFDUixJQUFJLEVBQUUsT0FBTztvQkFDYixLQUFLLEVBQUUsUUFBUTtpQkFDbEI7YUFDQSxDQUFDLENBQUM7UUFDSixDQUFDLEVBQ0gsVUFBQyxLQUFLO1lBQ0osS0FBSyxDQUFDLHNFQUFzRSxDQUFDLENBQUM7UUFDaEYsQ0FBQyxDQUFDLENBQUM7SUFFTixDQUFDO0lBRUQsZ0NBQU0sR0FBTjtRQUNFLEtBQUssQ0FBQyxnQ0FBZ0MsQ0FBQyxDQUFDO0lBQzFDLENBQUM7SUFFRCx1Q0FBYSxHQUFiO1FBQ0UsSUFBSSxDQUFDLFdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUM7SUFDdkMsQ0FBQztJQTNGVSxlQUFlO1FBUDNCLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsUUFBUTtZQUNsQixTQUFTLEVBQUUsQ0FBQyxtQ0FBVyxFQUFDLDBCQUFXLENBQUM7WUFDcEMsV0FBVyxFQUFFLHNDQUFzQztZQUNuRCxTQUFTLEVBQUUsQ0FBQyxzQ0FBc0MsQ0FBQztTQUNwRCxDQUFDO3lDQVdpQyxtQ0FBVyxFQUFnQixXQUFJLEVBQXVCLDBCQUFXO1lBQ3BGLHdCQUFpQixFQUE0Qix5QkFBZ0I7T0FWaEUsZUFBZSxDQTRGM0I7SUFBRCxzQkFBQztDQUFBLEFBNUZELElBNEZDO0FBNUZZLDBDQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LEVsZW1lbnRSZWYsIE9uSW5pdCwgVmlld0NoaWxkLCBDaGFuZ2VEZXRlY3RvclJlZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO2ltcG9ydCB7IFVzZXIgfSBmcm9tIFwiLi4vLi4veC1zaGFyZWQvYXBwL3NpZ25pbi9zaWduaW4tY29tbW9uLnZpZXctbW9kZWxcIjtcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSBcIi4uLy4uL3gtc2hhcmVkL2FwcC9zaWduaW4vc2lnbmluLWNvbW1vbi5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBQYWdlIH0gZnJvbSBcInVpL3BhZ2VcIjtcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tIFwicnhqcy9SeFwiO1xuaW1wb3J0IFwicnhqcy9hZGQvb3BlcmF0b3IvZG9cIjtcbmltcG9ydCB7IEFic29sdXRlTGF5b3V0IH0gZnJvbSBcInVpL2xheW91dHMvYWJzb2x1dGUtbGF5b3V0XCI7XG5pbXBvcnQgeyBCdXR0b24gfSBmcm9tIFwidWkvYnV0dG9uXCI7XG5pbXBvcnQgeyBzY3JlZW4gfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy9wbGF0Zm9ybVwiO1xuaW1wb3J0IHsgSXRlbSB9IGZyb20gXCIuLi8uLi94LXNoYXJlZC9wcm92aWRlcnMvc2xpZGVJdGVtL2l0ZW1cIjtcbmltcG9ydCB7IEl0ZW1TZXJ2aWNlIH0gZnJvbSBcIi4uLy4uL3gtc2hhcmVkL3Byb3ZpZGVycy9zbGlkZUl0ZW0vaXRlbS5zZXJ2aWNlXCI7XG5pbXBvcnQge1JvdXRlckV4dGVuc2lvbnN9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcblxuaW1wb3J0ICogYXMgYXBwU2V0dGluZ3MgZnJvbSBcImFwcGxpY2F0aW9uLXNldHRpbmdzXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogXCJzaWduaW5cIixcbiAgcHJvdmlkZXJzOiBbVXNlclNlcnZpY2UsSXRlbVNlcnZpY2VdLFxuICB0ZW1wbGF0ZVVybDogXCJtb2R1bGVzL3NpZ25pbi9zaWduaW4uY29tcG9uZW50Lmh0bWxcIixcbiAgc3R5bGVVcmxzOiBbXCJtb2R1bGVzL3NpZ25pbi9zaWduaW4uY29tcG9uZW50LnNjc3NcIl1cbn0pXG5cbmV4cG9ydCBjbGFzcyBTaWduaW5Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICB1c2VyOiBVc2VyO1xuICBpc0xvZ2dpbmdJbiA9IHRydWU7XG4gIHBhc3N3b3JkOiBzdHJpbmc7XG5cbiAgc2hvd0ludHJvID0gZmFsc2U7XG4gIGl0ZW1zOiBJdGVtW107XG4gIGJvdHRvbUNvbnRhaW5lcjogQnV0dG9uOyAgIFxuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgdXNlclNlcnZpY2U6IFVzZXJTZXJ2aWNlLCBwcml2YXRlIHBhZ2U6IFBhZ2UsIHByaXZhdGUgaXRlbVNlcnZpY2U6IEl0ZW1TZXJ2aWNlLFxuICAgcHJpdmF0ZSBjZHI6IENoYW5nZURldGVjdG9yUmVmLCBwcml2YXRlIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnMpIHtcbiAgICB0aGlzLnVzZXIgPSBuZXcgVXNlcigpO1xuICB9XG5cbiAgbmdBZnRlclZpZXdJbml0KCl7XG4gICAgIHZhciBib29sVmFsdWUgPSBhcHBTZXR0aW5ncy5nZXRCb29sZWFuKFwiYXBwRmlyc3RSdW5cIiwgZmFsc2UpO1xuICAgICBpZighYm9vbFZhbHVlKXtcbiAgICAgICAgIHRoaXMuaXRlbXMgPSB0aGlzLml0ZW1TZXJ2aWNlLmdldEl0ZW1zKCk7XG4gICAgICAgICB0aGlzLnNob3dJbnRybyA9IHRydWU7XG4gICAgfSAgXG4gICB0aGlzLmNkci5kZXRlY3RDaGFuZ2VzKCk7XG4gfVxuXG4gc2hvd0xvZ2luKCl7ICAgICBcbiAgICBhcHBTZXR0aW5ncy5zZXRCb29sZWFuKFwiYXBwRmlyc3RSdW5cIiwgdHJ1ZSk7XG4gICAgdGhpcy5zaG93SW50cm8gPSBmYWxzZTtcbiB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgdGhpcy5wYWdlLmFjdGlvbkJhckhpZGRlbiA9IHRydWU7XG4gIH1cblxuICBzdWJtaXQoKSB7XG4gICAgaWYgKHRoaXMuaXNMb2dnaW5nSW4pIHtcbiAgICAgIHRoaXMuc2lnbkluKCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGlmICh0aGlzLnVzZXIuaXNWYWxpZEVtYWlsKCkpIHtcbiAgICAgICAgdGhpcy5zaWduVXAoKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGFsZXJ0KFwiUGxlYXNlIGVudGVyIGEgdmFsaWQgZW1haWwgYWRkcmVzcy5cIik7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgc2lnbkluKCkge1xuICAgIC8vICoqKioqKioqKioqKioqKioqSGFyZGNvZGVkIHZlcnNpb25cbiAgICAvLyBpZiAodGhpcy51c2VyLnVuYW1lID09IFwibGlxdWlkaHViXCIgJiYgdGhpcy51c2VyLnBhc3N3b3JkID09IFwibGlxdWlkaHViXCIpIHtcbiAgICAvLyAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvaG9tZVwiXSwge1xuICAgIC8vICAgICBjbGVhckhpc3Rvcnk6IHRydWUsXG4gICAgLy8gICAgIHRyYW5zaXRpb246IHtcbiAgICAvLyAgICAgICAgIG5hbWU6IFwic2xpZGVcIixcbiAgICAvLyAgICAgICAgIGN1cnZlOiBcImxpbmVhclwiXG4gICAgLy8gICAgIH1cbiAgICAvLyB9KTtcbiAgICAvLyB9IGVsc2Uge1xuICAgIC8vICAgYWxlcnQoXCJVbmZvcnR1bmF0ZWx5IHdlIGNvdWxkIG5vdCBmaW5kIHlvdXIgYWNjb3VudC5cIik7XG4gICAgLy8gfVxuXG4gICAgLy8gKioqKioqKioqKioqKioqKioqR2V0IHRoZSBkYXRhIGZyb20gYmFja2VuZFxuICAgdGhpcy51c2VyU2VydmljZS5sb2dpbih0aGlzLnVzZXIpXG4gICAuc3Vic2NyaWJlKCByZXNwb25zZSA9PiB7XG4gICAgICAgIHRoaXMuZ2V0VXNlckRhdGEoKTtcbiAgICAgIH0sXG4gICAgKGVycm9yKSA9PiB7XG4gICAgICBhbGVydChcIlVuZm9ydHVuYXRlbHkgd2UgY291bGQgbm90IGZpbmQgeW91ciBhY2NvdW50LlwiKTtcbiAgICB9KTtcbiAgfSBcblxuICBnZXRVc2VyRGF0YSgpIHtcbiAgICB0aGlzLnVzZXJTZXJ2aWNlLmdldFVzZXJEYXRhKClcbiAgICAuc3Vic2NyaWJlKCByZXNwb25zZSA9PiB7XG4gICAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvaG9tZVwiXSwge1xuICAgICAgICBjbGVhckhpc3Rvcnk6IHRydWUsXG4gICAgICAgIHRyYW5zaXRpb246IHtcbiAgICAgICAgICAgIG5hbWU6IFwic2xpZGVcIixcbiAgICAgICAgICAgIGN1cnZlOiBcImxpbmVhclwiXG4gICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICAgfSxcbiAgICAgKGVycm9yKSA9PiB7XG4gICAgICAgYWxlcnQoXCJVbmZvcnR1bmF0ZWx5IHRoZXJlIGlzIHNvbWUgaXNzdWUuIFBsZWFzZSB0cnkgYWdhaW4gYWZ0ZXIgc29tZSB0aW1lLlwiKTtcbiAgICAgfSk7XG4gXG4gIH1cbiAgXG4gIHNpZ25VcCgpIHtcbiAgICBhbGVydChcIldlIHdpbGwgZ2V0IGJhY2sgdG8geW91IHNvb24hIVwiKTtcbiAgfVxuXG4gIHRvZ2dsZURpc3BsYXkoKSB7XG4gICAgdGhpcy5pc0xvZ2dpbmdJbiA9ICF0aGlzLmlzTG9nZ2luZ0luO1xuICB9XG59Il19