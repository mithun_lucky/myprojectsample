import { NgModule } from '@angular/core';
import { NativeScriptModule } from 'nativescript-angular/nativescript.module';
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptUISideDrawerModule } from "nativescript-pro-ui/sidedrawer/angular";

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { SigninComponent } from './signin/signin.component'
import { AppRoutingModule } from './app-routing.module';
import { SlidesModule } from 'nativescript-ngx-slides';
import { Adapter } from "../x-shared/providers/adapter";
import { SessionData } from "../x-shared/providers/sessionData";
import { IfAndroidDirective, IfIosDirective } from "./utils/if-platform.directive";
import { UtilityComponent } from "./utils/utilities";

import { VideoListComponent } from './video-list/video-list.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SigninComponent,
    IfAndroidDirective,
    IfIosDirective,
    VideoListComponent
  ],
  imports: [
    NativeScriptModule,
    AppRoutingModule,
    SlidesModule,
    NativeScriptFormsModule,
    NativeScriptHttpModule,
    NativeScriptUISideDrawerModule
  ],
  bootstrap: [AppComponent],
  providers: [Adapter, SessionData, UtilityComponent]
})
export class AppModule { }
