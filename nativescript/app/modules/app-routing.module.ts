import { NgModule } from '@angular/core';
import { NativeScriptRouterModule } from 'nativescript-angular/router';

import { HomeComponent } from './home/home.component';
import { SigninComponent } from './signin/signin.component';
import { VideoListComponent } from './video-list/video-list.component';

@NgModule({
  imports: [
    NativeScriptRouterModule.forRoot([
      { path: '', pathMatch: 'full', component: SigninComponent },
      { path: 'home', pathMatch: 'full', component: HomeComponent },
      { path: 'video-list', pathMatch: 'full', component: VideoListComponent }
    ])
  ],
  exports: [NativeScriptRouterModule]
})
export class AppRoutingModule {

}
