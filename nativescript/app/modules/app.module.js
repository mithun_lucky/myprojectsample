"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_module_1 = require("nativescript-angular/nativescript.module");
var http_1 = require("nativescript-angular/http");
var forms_1 = require("nativescript-angular/forms");
var app_component_1 = require("./app.component");
var home_component_1 = require("./home/home.component");
var signin_component_1 = require("./signin/signin.component");
var app_routing_module_1 = require("./app-routing.module");
var nativescript_ngx_slides_1 = require("nativescript-ngx-slides");
var adapter_1 = require("../x-shared/providers/adapter");
var sessionData_1 = require("../x-shared/providers/sessionData");
var if_platform_directive_1 = require("./utils/if-platform.directive");
var video_list_component_1 = require("./video-list/video-list.component");
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                home_component_1.HomeComponent,
                signin_component_1.SigninComponent,
                if_platform_directive_1.IfAndroidDirective,
                if_platform_directive_1.IfIosDirective,
                video_list_component_1.VideoListComponent
            ],
            imports: [
                nativescript_module_1.NativeScriptModule,
                app_routing_module_1.AppRoutingModule,
                nativescript_ngx_slides_1.SlidesModule,
                forms_1.NativeScriptFormsModule,
                http_1.NativeScriptHttpModule
            ],
            bootstrap: [app_component_1.AppComponent],
            providers: [adapter_1.Adapter, sessionData_1.SessionData]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFwcC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBeUM7QUFDekMsZ0ZBQThFO0FBQzlFLGtEQUFtRTtBQUNuRSxvREFBcUU7QUFFckUsaURBQStDO0FBQy9DLHdEQUFzRDtBQUN0RCw4REFBMkQ7QUFDM0QsMkRBQXdEO0FBQ3hELG1FQUF1RDtBQUN2RCx5REFBd0Q7QUFDeEQsaUVBQWdFO0FBQ2hFLHVFQUFtRjtBQUVuRiwwRUFBdUU7QUFxQnZFO0lBQUE7SUFBeUIsQ0FBQztJQUFiLFNBQVM7UUFuQnJCLGVBQVEsQ0FBQztZQUNSLFlBQVksRUFBRTtnQkFDWiw0QkFBWTtnQkFDWiw4QkFBYTtnQkFDYixrQ0FBZTtnQkFDZiwwQ0FBa0I7Z0JBQ2xCLHNDQUFjO2dCQUNkLHlDQUFrQjthQUNuQjtZQUNELE9BQU8sRUFBRTtnQkFDUCx3Q0FBa0I7Z0JBQ2xCLHFDQUFnQjtnQkFDaEIsc0NBQVk7Z0JBQ1osK0JBQXVCO2dCQUN2Qiw2QkFBc0I7YUFDdkI7WUFDRCxTQUFTLEVBQUUsQ0FBQyw0QkFBWSxDQUFDO1lBQ3pCLFNBQVMsRUFBRSxDQUFDLGlCQUFPLEVBQUUseUJBQVcsQ0FBQztTQUNsQyxDQUFDO09BQ1csU0FBUyxDQUFJO0lBQUQsZ0JBQUM7Q0FBQSxBQUExQixJQUEwQjtBQUFiLDhCQUFTIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdE1vZHVsZSB9IGZyb20gJ25hdGl2ZXNjcmlwdC1hbmd1bGFyL25hdGl2ZXNjcmlwdC5tb2R1bGUnO1xuaW1wb3J0IHsgTmF0aXZlU2NyaXB0SHR0cE1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9odHRwXCI7XG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRGb3Jtc01vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9mb3Jtc1wiO1xuXG5pbXBvcnQgeyBBcHBDb21wb25lbnQgfSBmcm9tICcuL2FwcC5jb21wb25lbnQnO1xuaW1wb3J0IHsgSG9tZUNvbXBvbmVudCB9IGZyb20gJy4vaG9tZS9ob21lLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBTaWduaW5Db21wb25lbnQgfSBmcm9tICcuL3NpZ25pbi9zaWduaW4uY29tcG9uZW50J1xuaW1wb3J0IHsgQXBwUm91dGluZ01vZHVsZSB9IGZyb20gJy4vYXBwLXJvdXRpbmcubW9kdWxlJztcbmltcG9ydCB7IFNsaWRlc01vZHVsZSB9IGZyb20gJ25hdGl2ZXNjcmlwdC1uZ3gtc2xpZGVzJztcbmltcG9ydCB7IEFkYXB0ZXIgfSBmcm9tIFwiLi4veC1zaGFyZWQvcHJvdmlkZXJzL2FkYXB0ZXJcIjtcbmltcG9ydCB7IFNlc3Npb25EYXRhIH0gZnJvbSBcIi4uL3gtc2hhcmVkL3Byb3ZpZGVycy9zZXNzaW9uRGF0YVwiO1xuaW1wb3J0IHsgSWZBbmRyb2lkRGlyZWN0aXZlLCBJZklvc0RpcmVjdGl2ZSB9IGZyb20gXCIuL3V0aWxzL2lmLXBsYXRmb3JtLmRpcmVjdGl2ZVwiO1xuXG5pbXBvcnQgeyBWaWRlb0xpc3RDb21wb25lbnQgfSBmcm9tICcuL3ZpZGVvLWxpc3QvdmlkZW8tbGlzdC5jb21wb25lbnQnO1xuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtcbiAgICBBcHBDb21wb25lbnQsXG4gICAgSG9tZUNvbXBvbmVudCxcbiAgICBTaWduaW5Db21wb25lbnQsXG4gICAgSWZBbmRyb2lkRGlyZWN0aXZlLFxuICAgIElmSW9zRGlyZWN0aXZlLFxuICAgIFZpZGVvTGlzdENvbXBvbmVudFxuICBdLFxuICBpbXBvcnRzOiBbXG4gICAgTmF0aXZlU2NyaXB0TW9kdWxlLFxuICAgIEFwcFJvdXRpbmdNb2R1bGUsXG4gICAgU2xpZGVzTW9kdWxlLFxuICAgIE5hdGl2ZVNjcmlwdEZvcm1zTW9kdWxlLFxuICAgIE5hdGl2ZVNjcmlwdEh0dHBNb2R1bGVcbiAgXSxcbiAgYm9vdHN0cmFwOiBbQXBwQ29tcG9uZW50XSxcbiAgcHJvdmlkZXJzOiBbQWRhcHRlciwgU2Vzc2lvbkRhdGFdXG59KVxuZXhwb3J0IGNsYXNzIEFwcE1vZHVsZSB7IH1cbiJdfQ==