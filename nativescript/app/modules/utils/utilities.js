"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var UtilityComponent = (function () {
    function UtilityComponent(routerExtensions) {
        this.routerExtensions = routerExtensions;
    }
    UtilityComponent.prototype.forwardNavigation = function (path, clearHistory) {
        this.routerExtensions.navigate([path], {
            clearHistory: clearHistory,
            transition: {
                name: "slide",
                curve: "linear"
            }
        });
    };
    UtilityComponent.prototype.backwardNavigation = function (path, clearHistory) {
        this.routerExtensions.navigate([path], {
            clearHistory: clearHistory,
            transition: {
                name: "slideRight",
                curve: "linear"
            }
        });
    };
    UtilityComponent.prototype.backToPreviousPage = function () {
        this.routerExtensions.backToPreviousPage();
    };
    UtilityComponent = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [router_1.RouterExtensions])
    ], UtilityComponent);
    return UtilityComponent;
}());
exports.UtilityComponent = UtilityComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXRpbGl0aWVzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidXRpbGl0aWVzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTJDO0FBQzNDLHNEQUE2RDtBQUc3RDtJQUNJLDBCQUFvQixnQkFBa0M7UUFBbEMscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtJQUN0RCxDQUFDO0lBRUQsNENBQWlCLEdBQWpCLFVBQWtCLElBQVksRUFBRSxZQUFxQjtRQUNqRCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDbkMsWUFBWSxFQUFFLFlBQVk7WUFDMUIsVUFBVSxFQUFFO2dCQUNSLElBQUksRUFBRSxPQUFPO2dCQUNiLEtBQUssRUFBRSxRQUFRO2FBQ2xCO1NBQ0osQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDZDQUFrQixHQUFsQixVQUFtQixJQUFZLEVBQUUsWUFBcUI7UUFDbEQsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ25DLFlBQVksRUFBRSxZQUFZO1lBQzFCLFVBQVUsRUFBRTtnQkFDUixJQUFJLEVBQUUsWUFBWTtnQkFDbEIsS0FBSyxFQUFFLFFBQVE7YUFDbEI7U0FDSixDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsNkNBQWtCLEdBQWxCO1FBQ0ksSUFBSSxDQUFDLGdCQUFnQixDQUFDLGtCQUFrQixFQUFFLENBQUM7SUFDL0MsQ0FBQztJQTFCUSxnQkFBZ0I7UUFENUIsaUJBQVUsRUFBRTt5Q0FFNkIseUJBQWdCO09BRDdDLGdCQUFnQixDQTJCNUI7SUFBRCx1QkFBQztDQUFBLEFBM0JELElBMkJDO0FBM0JZLDRDQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHtSb3V0ZXJFeHRlbnNpb25zfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBVdGlsaXR5Q29tcG9uZW50IHtcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnMpIHtcbiAgICB9XG5cbiAgICBmb3J3YXJkTmF2aWdhdGlvbihwYXRoOiBzdHJpbmcsIGNsZWFySGlzdG9yeTogYm9vbGVhbikge1xuICAgICAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW3BhdGhdLCB7XG4gICAgICAgICAgICBjbGVhckhpc3Rvcnk6IGNsZWFySGlzdG9yeSxcbiAgICAgICAgICAgIHRyYW5zaXRpb246IHtcbiAgICAgICAgICAgICAgICBuYW1lOiBcInNsaWRlXCIsXG4gICAgICAgICAgICAgICAgY3VydmU6IFwibGluZWFyXCJcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgYmFja3dhcmROYXZpZ2F0aW9uKHBhdGg6IHN0cmluZywgY2xlYXJIaXN0b3J5OiBib29sZWFuKSB7XG4gICAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbcGF0aF0sIHtcbiAgICAgICAgICAgIGNsZWFySGlzdG9yeTogY2xlYXJIaXN0b3J5LFxuICAgICAgICAgICAgdHJhbnNpdGlvbjoge1xuICAgICAgICAgICAgICAgIG5hbWU6IFwic2xpZGVSaWdodFwiLFxuICAgICAgICAgICAgICAgIGN1cnZlOiBcImxpbmVhclwiXG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGJhY2tUb1ByZXZpb3VzUGFnZSgpIHtcbiAgICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLmJhY2tUb1ByZXZpb3VzUGFnZSgpO1xuICAgIH1cbn0iXX0=