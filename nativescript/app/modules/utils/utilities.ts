import { Injectable } from "@angular/core";
import {RouterExtensions} from "nativescript-angular/router";

@Injectable()
export class UtilityComponent {
    constructor(private routerExtensions: RouterExtensions) {
    }

    forwardNavigation(path: string, clearHistory: boolean) {
        this.routerExtensions.navigate([path], {
            clearHistory: clearHistory,
            transition: {
                name: "slide",
                curve: "linear"
            }
        });
    }

    backwardNavigation(path: string, clearHistory: boolean) {
        this.routerExtensions.navigate([path], {
            clearHistory: clearHistory,
            transition: {
                name: "slideRight",
                curve: "linear"
            }
        });
    }

    backToPreviousPage() {
        this.routerExtensions.backToPreviousPage();
    }
}