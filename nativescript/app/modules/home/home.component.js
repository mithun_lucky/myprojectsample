"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var home_common_view_model_1 = require("../../x-shared/app/home/home-common.view-model");
var router_1 = require("nativescript-angular/router");
var HomeComponent = (function () {
    function HomeComponent(cvm, routerExtensions) {
        this.cvm = cvm;
        this.routerExtensions = routerExtensions;
    }
    HomeComponent.prototype.showVideoPage = function () {
        this.routerExtensions.navigate(["/video-list"], {
            transition: {
                name: "slide",
                curve: "linear"
            }
        });
    };
    HomeComponent = __decorate([
        core_1.Component({
            selector: 'home',
            templateUrl: 'modules/home/home.component.html',
            styleUrls: ['modules/home/home.component.scss'],
            providers: [home_common_view_model_1.HomeCommonViewModel]
        }),
        __metadata("design:paramtypes", [home_common_view_model_1.HomeCommonViewModel, router_1.RouterExtensions])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9tZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJob21lLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUEwQztBQUUxQyx5RkFBcUY7QUFHckYsc0RBQStEO0FBUy9EO0lBRUUsdUJBQW1CLEdBQXdCLEVBQVUsZ0JBQWtDO1FBQXBFLFFBQUcsR0FBSCxHQUFHLENBQXFCO1FBQVUscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtJQUV2RixDQUFDO0lBRU8scUNBQWEsR0FBckI7UUFDRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsYUFBYSxDQUFDLEVBQUU7WUFDOUMsVUFBVSxFQUFFO2dCQUNSLElBQUksRUFBRSxPQUFPO2dCQUNiLEtBQUssRUFBRSxRQUFRO2FBQ2xCO1NBQ0osQ0FBQyxDQUFDO0lBQ0gsQ0FBQztJQWJVLGFBQWE7UUFOekIsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxNQUFNO1lBQ2hCLFdBQVcsRUFBRSxrQ0FBa0M7WUFDL0MsU0FBUyxFQUFFLENBQUMsa0NBQWtDLENBQUM7WUFDL0MsU0FBUyxFQUFFLENBQUMsNENBQW1CLENBQUM7U0FDakMsQ0FBQzt5Q0FHd0IsNENBQW1CLEVBQTRCLHlCQUFnQjtPQUY1RSxhQUFhLENBY3pCO0lBQUQsb0JBQUM7Q0FBQSxBQWRELElBY0M7QUFkWSxzQ0FBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBIb21lQ29tbW9uVmlld01vZGVsIH0gZnJvbSAnLi4vLi4veC1zaGFyZWQvYXBwL2hvbWUvaG9tZS1jb21tb24udmlldy1tb2RlbCc7XG5pbXBvcnQgeyBJZkFuZHJvaWREaXJlY3RpdmUsIElmSW9zRGlyZWN0aXZlIH0gZnJvbSBcIi4uL3V0aWxzL2lmLXBsYXRmb3JtLmRpcmVjdGl2ZVwiO1xuXG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xuaW1wb3J0ICogYXMgZnJhbWVNb2R1bGUgZnJvbSBcInVpL2ZyYW1lXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2hvbWUnLFxuICB0ZW1wbGF0ZVVybDogJ21vZHVsZXMvaG9tZS9ob21lLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJ21vZHVsZXMvaG9tZS9ob21lLmNvbXBvbmVudC5zY3NzJ10sXG4gIHByb3ZpZGVyczogW0hvbWVDb21tb25WaWV3TW9kZWxdXG59KVxuZXhwb3J0IGNsYXNzIEhvbWVDb21wb25lbnQge1xuXG4gIGNvbnN0cnVjdG9yKHB1YmxpYyBjdm06IEhvbWVDb21tb25WaWV3TW9kZWwsIHByaXZhdGUgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucykge1xuXG4gIH1cblxuICBwcml2YXRlIHNob3dWaWRlb1BhZ2UoKSB7XG4gICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi92aWRlby1saXN0XCJdLCB7XG4gICAgICB0cmFuc2l0aW9uOiB7XG4gICAgICAgICAgbmFtZTogXCJzbGlkZVwiLFxuICAgICAgICAgIGN1cnZlOiBcImxpbmVhclwiXG4gICAgICB9XG4gIH0pO1xuICB9XG59XG4iXX0=