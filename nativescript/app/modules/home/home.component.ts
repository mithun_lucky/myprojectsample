import { Component, ViewChild, OnInit, AfterViewInit, ChangeDetectorRef } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { Observable } from "data/observable";
import { RadSideDrawerComponent, SideDrawerType } from "nativescript-pro-ui/sidedrawer/angular";
import { RadSideDrawer } from 'nativescript-pro-ui/sidedrawer';
import { Page } from "ui/page";
import { ActionItem } from "ui/action-bar";
import * as frameModule from "ui/frame";
import * as dialogs from "ui/dialogs";
import { HomeCommonViewModel } from '../../x-shared/app/home/home-common.view-model';
import { SessionData } from '../../x-shared/providers/sessionData';
import { IfAndroidDirective, IfIosDirective } from "../utils/if-platform.directive";
import { UtilityComponent } from "../utils/utilities";

@Component({
  selector: 'home',
  templateUrl: 'modules/home/home.component.html',
  styleUrls: ['modules/home/home.component.scss'],
  providers: [HomeCommonViewModel]
})
export class HomeComponent {
  private drawer: RadSideDrawer;

  @ViewChild(RadSideDrawerComponent) public drawerComponent: RadSideDrawerComponent;
  constructor(public cvm: HomeCommonViewModel, private routerExtensions: RouterExtensions,
    private changeDetectionRef: ChangeDetectorRef, private sessionData: SessionData, 
    private utilityHandler: UtilityComponent) {
  }

  private showVideoPage() {
    this.utilityHandler.forwardNavigation("/video-list", false);
  }

  ngAfterViewInit() {
    this.drawer = this.drawerComponent.sideDrawer;
    this.changeDetectionRef.detectChanges();
  }

  toggleDrawerState() {
    this.drawer.toggleDrawerState();
  }

  logout() {
    dialogs.confirm({
      title: "LogOut",
      message: "Are you sure you want to log out",
      okButtonText: "Yes",
      cancelButtonText: "No"
    }).then(result => {
      if (result) {
        this.sessionData.clearSessionData();
        // this.utilityHandler.backwardNavigation("/signin", true);
      }
    });
  }
}
