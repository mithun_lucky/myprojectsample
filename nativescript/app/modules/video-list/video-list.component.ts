import { Component, OnInit } from '@angular/core';
import { Video } from "../../x-shared/app/video/video-model";
import { Videos } from "../../x-shared/app/video/video-temp";

import {registerElement} from "nativescript-angular/element-registry";
registerElement("VideoPlayer", () => require("nativescript-videoplayer").Video);

@Component({
  selector: "video-list",
  templateUrl: "modules/video-list/video-list.component.html",
  styleUrls: ["modules/video-list/video-list.component.scss"]
})

export class VideoListComponent implements OnInit {

  private videoSrc: string;
  private videos: Video[];

  constructor() {
    this.videos = Videos;
  }

  ngOnInit(): void {
    this.videoSrc = this.videos[0].url;
    console.log(this.videos)
   }

  public onItemTap(args) {
    this.videoSrc = this.videos[args.index].url;
  }

}
