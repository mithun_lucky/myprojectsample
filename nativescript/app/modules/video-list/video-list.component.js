"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var video_temp_1 = require("../../x-shared/app/video/video-temp");
var element_registry_1 = require("nativescript-angular/element-registry");
element_registry_1.registerElement("VideoPlayer", function () { return require("nativescript-videoplayer").Video; });
var VideoListComponent = (function () {
    function VideoListComponent() {
        this.videos = video_temp_1.Videos;
    }
    VideoListComponent.prototype.ngOnInit = function () {
        this.videoSrc = this.videos[0].url;
        console.log(this.videos);
    };
    VideoListComponent.prototype.onItemTap = function (args) {
        this.videoSrc = this.videos[args.index].url;
    };
    VideoListComponent = __decorate([
        core_1.Component({
            selector: "video-list",
            templateUrl: "modules/video-list/video-list.component.html",
            styleUrls: ["modules/video-list/video-list.component.scss"]
        }),
        __metadata("design:paramtypes", [])
    ], VideoListComponent);
    return VideoListComponent;
}());
exports.VideoListComponent = VideoListComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlkZW8tbGlzdC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ2aWRlby1saXN0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQUVsRCxrRUFBNkQ7QUFFN0QsMEVBQXNFO0FBQ3RFLGtDQUFlLENBQUMsYUFBYSxFQUFFLGNBQU0sT0FBQSxPQUFPLENBQUMsMEJBQTBCLENBQUMsQ0FBQyxLQUFLLEVBQXpDLENBQXlDLENBQUMsQ0FBQztBQVFoRjtJQUtFO1FBQ0UsSUFBSSxDQUFDLE1BQU0sR0FBRyxtQkFBTSxDQUFDO0lBQ3ZCLENBQUM7SUFFRCxxQ0FBUSxHQUFSO1FBQ0UsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQztRQUNuQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQTtJQUN6QixDQUFDO0lBRUssc0NBQVMsR0FBaEIsVUFBaUIsSUFBSTtRQUNuQixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsQ0FBQztJQUM5QyxDQUFDO0lBaEJVLGtCQUFrQjtRQU45QixnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLFlBQVk7WUFDdEIsV0FBVyxFQUFFLDhDQUE4QztZQUMzRCxTQUFTLEVBQUUsQ0FBQyw4Q0FBOEMsQ0FBQztTQUM1RCxDQUFDOztPQUVXLGtCQUFrQixDQWtCOUI7SUFBRCx5QkFBQztDQUFBLEFBbEJELElBa0JDO0FBbEJZLGdEQUFrQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBWaWRlbyB9IGZyb20gXCIuLi8uLi94LXNoYXJlZC9hcHAvdmlkZW8vdmlkZW8tbW9kZWxcIjtcbmltcG9ydCB7IFZpZGVvcyB9IGZyb20gXCIuLi8uLi94LXNoYXJlZC9hcHAvdmlkZW8vdmlkZW8tdGVtcFwiO1xuXG5pbXBvcnQge3JlZ2lzdGVyRWxlbWVudH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL2VsZW1lbnQtcmVnaXN0cnlcIjtcbnJlZ2lzdGVyRWxlbWVudChcIlZpZGVvUGxheWVyXCIsICgpID0+IHJlcXVpcmUoXCJuYXRpdmVzY3JpcHQtdmlkZW9wbGF5ZXJcIikuVmlkZW8pO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6IFwidmlkZW8tbGlzdFwiLFxuICB0ZW1wbGF0ZVVybDogXCJtb2R1bGVzL3ZpZGVvLWxpc3QvdmlkZW8tbGlzdC5jb21wb25lbnQuaHRtbFwiLFxuICBzdHlsZVVybHM6IFtcIm1vZHVsZXMvdmlkZW8tbGlzdC92aWRlby1saXN0LmNvbXBvbmVudC5zY3NzXCJdXG59KVxuXG5leHBvcnQgY2xhc3MgVmlkZW9MaXN0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBwcml2YXRlIHZpZGVvU3JjOiBzdHJpbmc7XG4gIHByaXZhdGUgdmlkZW9zOiBWaWRlb1tdO1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMudmlkZW9zID0gVmlkZW9zO1xuICB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgdGhpcy52aWRlb1NyYyA9IHRoaXMudmlkZW9zWzBdLnVybDtcbiAgICBjb25zb2xlLmxvZyh0aGlzLnZpZGVvcylcbiAgIH1cblxuICBwdWJsaWMgb25JdGVtVGFwKGFyZ3MpIHtcbiAgICB0aGlzLnZpZGVvU3JjID0gdGhpcy52aWRlb3NbYXJncy5pbmRleF0udXJsO1xuICB9XG5cbn1cbiJdfQ==