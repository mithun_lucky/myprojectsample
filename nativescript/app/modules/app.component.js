"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var observable_1 = require("tns-core-modules/data/observable");
var pushPlugin = require("nativescript-push-notifications");
var AppComponent = (function (_super) {
    __extends(AppComponent, _super);
    function AppComponent() {
        var _this = _super.call(this) || this;
        _this.pushSettings = {
            // Android settings
            senderID: "858200201480",
            notificationCallbackAndroid: function (stringifiedData, fcmNotification) {
                var notificationBody = fcmNotification && fcmNotification.getBody();
                _this.updateMessage("Message received!\n" + notificationBody + "\n" + stringifiedData);
            },
            // iOS settings
            badge: true,
            sound: true,
            alert: true,
            notificationCallbackIOS: function (message) {
                _this.updateMessage("Message received!\n" + JSON.stringify(message));
            }
        };
        // pushPlugin.register({ senderID: '858200201480' }, (token: String) => {
        //     // alert("Device registered. Access token: " + token);;
        //   }, function() { });
        // pushPlugin.onMessageReceived((stringifiedData: String, fcmNotification: any) => {
        //       const notificationBody = fcmNotification && fcmNotification.getBody();
        //       alert("Message received!\n" + notificationBody + "\n" + stringifiedData);
        //   }); 
        _this.message = "";
        _this.updateMessage("App started.");
        var self = _this;
        _this.onRegisterButtonTap();
        return _this;
    }
    Object.defineProperty(AppComponent.prototype, "message", {
        get: function () {
            return this._message;
        },
        set: function (value) {
            if (this._message !== value) {
                this._message = value;
                this.notifyPropertyChange("message", value);
            }
        },
        enumerable: true,
        configurable: true
    });
    AppComponent.prototype.onCheckButtonTap = function () {
        var self = this;
        pushPlugin.areNotificationsEnabled(function (areEnabled) {
            self.updateMessage("Are Notifications enabled: " + !!areEnabled);
        });
    };
    AppComponent.prototype.onRegisterButtonTap = function () {
        var _this = this;
        var self = this;
        pushPlugin.register(this.pushSettings, function (token) {
            self.updateMessage("Device registered. Access token: " + token);
            // token displayed in console for easier copying and debugging durng development
            console.log("Device registered. Access token: " + token);
            if (pushPlugin.onMessageReceived) {
                pushPlugin.onMessageReceived(_this.pushSettings.notificationCallbackAndroid);
            }
            if (pushPlugin.registerUserNotificationSettings) {
                pushPlugin.registerUserNotificationSettings(function () {
                    self.updateMessage("Successfully registered for interactive push.");
                }, function (err) {
                    self.updateMessage("Error registering for interactive push: " + JSON.stringify(err));
                });
            }
        }, function (errorMessage) {
            self.updateMessage(JSON.stringify(errorMessage));
        });
    };
    AppComponent.prototype.updateMessage = function (text) {
        this.message += text + "\n";
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            template: "<page-router-outlet></page-router-outlet>"
        }),
        __metadata("design:paramtypes", [])
    ], AppComponent);
    return AppComponent;
}(observable_1.Observable));
exports.AppComponent = AppComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFwcC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMEM7QUFDMUMsK0RBQThEO0FBQzlELDREQUE4RDtBQU05RDtJQUFrQyxnQ0FBVTtJQXFCMUM7UUFBQSxZQUNFLGlCQUFPLFNBY1I7UUFuQ08sa0JBQVksR0FBRztZQUNqQixtQkFBbUI7WUFDbkIsUUFBUSxFQUFFLGNBQWM7WUFDeEIsMkJBQTJCLEVBQUUsVUFBQyxlQUF1QixFQUFFLGVBQW9CO2dCQUN2RSxJQUFNLGdCQUFnQixHQUFHLGVBQWUsSUFBSSxlQUFlLENBQUMsT0FBTyxFQUFFLENBQUM7Z0JBQ3RFLEtBQUksQ0FBQyxhQUFhLENBQUMscUJBQXFCLEdBQUcsZ0JBQWdCLEdBQUcsSUFBSSxHQUFHLGVBQWUsQ0FBQyxDQUFDO1lBQzFGLENBQUM7WUFFRCxlQUFlO1lBQ2YsS0FBSyxFQUFFLElBQUk7WUFDWCxLQUFLLEVBQUUsSUFBSTtZQUNYLEtBQUssRUFBRSxJQUFJO1lBQ1gsdUJBQXVCLEVBQUUsVUFBQyxPQUFZO2dCQUNsQyxLQUFJLENBQUMsYUFBYSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUN4RSxDQUFDO1NBQ0osQ0FBQztRQU9GLHlFQUF5RTtRQUN6RSw4REFBOEQ7UUFDOUQsd0JBQXdCO1FBRXRCLG9GQUFvRjtRQUNwRiwrRUFBK0U7UUFDL0Usa0ZBQWtGO1FBQ2xGLFNBQVM7UUFDVCxLQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztRQUNoQixLQUFJLENBQUMsYUFBYSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBRW5DLElBQUksSUFBSSxHQUFHLEtBQUksQ0FBQztRQUNoQixLQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQzs7SUFDakMsQ0FBQztJQUVBLHNCQUFJLGlDQUFPO2FBQVg7WUFDSyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUN6QixDQUFDO2FBRUQsVUFBWSxLQUFhO1lBQ3JCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLEtBQUssS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDMUIsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7Z0JBQ3RCLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDaEQsQ0FBQztRQUNMLENBQUM7OztPQVBBO0lBU0QsdUNBQWdCLEdBQWhCO1FBQ0ksSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLFVBQVUsQ0FBQyx1QkFBdUIsQ0FBQyxVQUFDLFVBQW1CO1lBQ25ELElBQUksQ0FBQyxhQUFhLENBQUMsNkJBQTZCLEdBQUcsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3JFLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDBDQUFtQixHQUFuQjtRQUFBLGlCQXFCQztRQXBCRyxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsVUFBVSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLFVBQUMsS0FBYTtZQUNqRCxJQUFJLENBQUMsYUFBYSxDQUFDLG1DQUFtQyxHQUFHLEtBQUssQ0FBQyxDQUFDO1lBQ2hFLGdGQUFnRjtZQUNoRixPQUFPLENBQUMsR0FBRyxDQUFDLG1DQUFtQyxHQUFHLEtBQUssQ0FBQyxDQUFDO1lBRXpELEVBQUUsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUM7Z0JBQy9CLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFJLENBQUMsWUFBWSxDQUFDLDJCQUEyQixDQUFDLENBQUM7WUFDaEYsQ0FBQztZQUVELEVBQUUsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxnQ0FBZ0MsQ0FBQyxDQUFDLENBQUM7Z0JBQzlDLFVBQVUsQ0FBQyxnQ0FBZ0MsQ0FBQztvQkFDeEMsSUFBSSxDQUFDLGFBQWEsQ0FBQywrQ0FBK0MsQ0FBQyxDQUFDO2dCQUN4RSxDQUFDLEVBQUUsVUFBQyxHQUFHO29CQUNILElBQUksQ0FBQyxhQUFhLENBQUMsMENBQTBDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUN6RixDQUFDLENBQUMsQ0FBQztZQUNQLENBQUM7UUFDTCxDQUFDLEVBQUUsVUFBQyxZQUFvQjtZQUNwQixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztRQUNyRCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFTyxvQ0FBYSxHQUFyQixVQUFzQixJQUFZO1FBQzlCLElBQUksQ0FBQyxPQUFPLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztJQUNoQyxDQUFDO0lBakZRLFlBQVk7UUFKeEIsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxRQUFRO1lBQ2xCLFFBQVEsRUFBRSwyQ0FBMkM7U0FDdEQsQ0FBQzs7T0FDVyxZQUFZLENBb0Z4QjtJQUFELG1CQUFDO0NBQUEsQUFwRkQsQ0FBa0MsdUJBQVUsR0FvRjNDO0FBcEZZLG9DQUFZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvZGF0YS9vYnNlcnZhYmxlXCI7XG5pbXBvcnQgKiBhcyBwdXNoUGx1Z2luIGZyb20gXCJuYXRpdmVzY3JpcHQtcHVzaC1ub3RpZmljYXRpb25zXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ215LWFwcCcsXG4gIHRlbXBsYXRlOiBgPHBhZ2Utcm91dGVyLW91dGxldD48L3BhZ2Utcm91dGVyLW91dGxldD5gXG59KVxuZXhwb3J0IGNsYXNzIEFwcENvbXBvbmVudCBleHRlbmRzIE9ic2VydmFibGUge1xuICBwcml2YXRlIHB1c2hTZXR0aW5ncyA9IHtcbiAgICAgICAgLy8gQW5kcm9pZCBzZXR0aW5nc1xuICAgICAgICBzZW5kZXJJRDogXCI4NTgyMDAyMDE0ODBcIiwgLy8gQW5kcm9pZDogUmVxdWlyZWQgc2V0dGluZyB3aXRoIHRoZSBzZW5kZXIvcHJvamVjdCBudW1iZXJcbiAgICAgICAgbm90aWZpY2F0aW9uQ2FsbGJhY2tBbmRyb2lkOiAoc3RyaW5naWZpZWREYXRhOiBTdHJpbmcsIGZjbU5vdGlmaWNhdGlvbjogYW55KSA9PiB7XG4gICAgICAgICAgICBjb25zdCBub3RpZmljYXRpb25Cb2R5ID0gZmNtTm90aWZpY2F0aW9uICYmIGZjbU5vdGlmaWNhdGlvbi5nZXRCb2R5KCk7XG4gICAgICAgICAgICB0aGlzLnVwZGF0ZU1lc3NhZ2UoXCJNZXNzYWdlIHJlY2VpdmVkIVxcblwiICsgbm90aWZpY2F0aW9uQm9keSArIFwiXFxuXCIgKyBzdHJpbmdpZmllZERhdGEpO1xuICAgICAgICB9LFxuXG4gICAgICAgIC8vIGlPUyBzZXR0aW5nc1xuICAgICAgICBiYWRnZTogdHJ1ZSwgLy8gRW5hYmxlIHNldHRpbmcgYmFkZ2UgdGhyb3VnaCBQdXNoIE5vdGlmaWNhdGlvblxuICAgICAgICBzb3VuZDogdHJ1ZSwgLy8gRW5hYmxlIHBsYXlpbmcgYSBzb3VuZFxuICAgICAgICBhbGVydDogdHJ1ZSwgLy8gRW5hYmxlIGNyZWF0aW5nIGEgYWxlcnRcbiAgICAgICAgbm90aWZpY2F0aW9uQ2FsbGJhY2tJT1M6IChtZXNzYWdlOiBhbnkpID0+IHtcbiAgICAgICAgICAgIHRoaXMudXBkYXRlTWVzc2FnZShcIk1lc3NhZ2UgcmVjZWl2ZWQhXFxuXCIgKyBKU09OLnN0cmluZ2lmeShtZXNzYWdlKSk7XG4gICAgICAgIH1cbiAgICB9O1xuXG4gICAgcHJpdmF0ZSBfY291bnRlcjogbnVtYmVyO1xuICAgIHByaXZhdGUgX21lc3NhZ2U6IHN0cmluZztcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICBzdXBlcigpO1xuICAgIC8vIHB1c2hQbHVnaW4ucmVnaXN0ZXIoeyBzZW5kZXJJRDogJzg1ODIwMDIwMTQ4MCcgfSwgKHRva2VuOiBTdHJpbmcpID0+IHtcbiAgICAvLyAgICAgLy8gYWxlcnQoXCJEZXZpY2UgcmVnaXN0ZXJlZC4gQWNjZXNzIHRva2VuOiBcIiArIHRva2VuKTs7XG4gICAgLy8gICB9LCBmdW5jdGlvbigpIHsgfSk7XG5cbiAgICAgIC8vIHB1c2hQbHVnaW4ub25NZXNzYWdlUmVjZWl2ZWQoKHN0cmluZ2lmaWVkRGF0YTogU3RyaW5nLCBmY21Ob3RpZmljYXRpb246IGFueSkgPT4ge1xuICAgICAgLy8gICAgICAgY29uc3Qgbm90aWZpY2F0aW9uQm9keSA9IGZjbU5vdGlmaWNhdGlvbiAmJiBmY21Ob3RpZmljYXRpb24uZ2V0Qm9keSgpO1xuICAgICAgLy8gICAgICAgYWxlcnQoXCJNZXNzYWdlIHJlY2VpdmVkIVxcblwiICsgbm90aWZpY2F0aW9uQm9keSArIFwiXFxuXCIgKyBzdHJpbmdpZmllZERhdGEpO1xuICAgICAgLy8gICB9KTsgXG4gICAgICB0aGlzLm1lc3NhZ2UgPSBcIlwiO1xuICAgICAgICB0aGlzLnVwZGF0ZU1lc3NhZ2UoXCJBcHAgc3RhcnRlZC5cIik7XG5cbiAgICAgICAgbGV0IHNlbGYgPSB0aGlzO1xuICAgICAgICB0aGlzLm9uUmVnaXN0ZXJCdXR0b25UYXAoKTtcbiAgfVxuXG4gICBnZXQgbWVzc2FnZSgpOiBzdHJpbmcge1xuICAgICAgICByZXR1cm4gdGhpcy5fbWVzc2FnZTtcbiAgICB9XG5cbiAgICBzZXQgbWVzc2FnZSh2YWx1ZTogc3RyaW5nKSB7XG4gICAgICAgIGlmICh0aGlzLl9tZXNzYWdlICE9PSB2YWx1ZSkge1xuICAgICAgICAgICAgdGhpcy5fbWVzc2FnZSA9IHZhbHVlO1xuICAgICAgICAgICAgdGhpcy5ub3RpZnlQcm9wZXJ0eUNoYW5nZShcIm1lc3NhZ2VcIiwgdmFsdWUpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgb25DaGVja0J1dHRvblRhcCgpIHtcbiAgICAgICAgbGV0IHNlbGYgPSB0aGlzO1xuICAgICAgICBwdXNoUGx1Z2luLmFyZU5vdGlmaWNhdGlvbnNFbmFibGVkKChhcmVFbmFibGVkOiBCb29sZWFuKSA9PiB7XG4gICAgICAgICAgICBzZWxmLnVwZGF0ZU1lc3NhZ2UoXCJBcmUgTm90aWZpY2F0aW9ucyBlbmFibGVkOiBcIiArICEhYXJlRW5hYmxlZCk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIG9uUmVnaXN0ZXJCdXR0b25UYXAoKSB7XG4gICAgICAgIGxldCBzZWxmID0gdGhpcztcbiAgICAgICAgcHVzaFBsdWdpbi5yZWdpc3Rlcih0aGlzLnB1c2hTZXR0aW5ncywgKHRva2VuOiBTdHJpbmcpID0+IHtcbiAgICAgICAgICAgIHNlbGYudXBkYXRlTWVzc2FnZShcIkRldmljZSByZWdpc3RlcmVkLiBBY2Nlc3MgdG9rZW46IFwiICsgdG9rZW4pO1xuICAgICAgICAgICAgLy8gdG9rZW4gZGlzcGxheWVkIGluIGNvbnNvbGUgZm9yIGVhc2llciBjb3B5aW5nIGFuZCBkZWJ1Z2dpbmcgZHVybmcgZGV2ZWxvcG1lbnRcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiRGV2aWNlIHJlZ2lzdGVyZWQuIEFjY2VzcyB0b2tlbjogXCIgKyB0b2tlbik7XG5cbiAgICAgICAgICAgIGlmIChwdXNoUGx1Z2luLm9uTWVzc2FnZVJlY2VpdmVkKSB7XG4gICAgICAgICAgICAgICAgcHVzaFBsdWdpbi5vbk1lc3NhZ2VSZWNlaXZlZCh0aGlzLnB1c2hTZXR0aW5ncy5ub3RpZmljYXRpb25DYWxsYmFja0FuZHJvaWQpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAocHVzaFBsdWdpbi5yZWdpc3RlclVzZXJOb3RpZmljYXRpb25TZXR0aW5ncykge1xuICAgICAgICAgICAgICAgIHB1c2hQbHVnaW4ucmVnaXN0ZXJVc2VyTm90aWZpY2F0aW9uU2V0dGluZ3MoKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICBzZWxmLnVwZGF0ZU1lc3NhZ2UoXCJTdWNjZXNzZnVsbHkgcmVnaXN0ZXJlZCBmb3IgaW50ZXJhY3RpdmUgcHVzaC5cIik7XG4gICAgICAgICAgICAgICAgfSwgKGVycikgPT4ge1xuICAgICAgICAgICAgICAgICAgICBzZWxmLnVwZGF0ZU1lc3NhZ2UoXCJFcnJvciByZWdpc3RlcmluZyBmb3IgaW50ZXJhY3RpdmUgcHVzaDogXCIgKyBKU09OLnN0cmluZ2lmeShlcnIpKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSwgKGVycm9yTWVzc2FnZTogU3RyaW5nKSA9PiB7XG4gICAgICAgICAgICBzZWxmLnVwZGF0ZU1lc3NhZ2UoSlNPTi5zdHJpbmdpZnkoZXJyb3JNZXNzYWdlKSk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHByaXZhdGUgdXBkYXRlTWVzc2FnZSh0ZXh0OiBTdHJpbmcpIHtcbiAgICAgICAgdGhpcy5tZXNzYWdlICs9IHRleHQgKyBcIlxcblwiO1xuICAgIH1cblxuXG59XG4iXX0=