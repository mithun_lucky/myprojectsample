"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var SessionData = (function () {
    function SessionData() {
        this.token = "";
    }
    SessionData.prototype.clearSessionData = function () {
        this.token = "";
    };
    SessionData = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], SessionData);
    return SessionData;
}());
exports.SessionData = SessionData;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2Vzc2lvbkRhdGEuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzZXNzaW9uRGF0YS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUEyQztBQUczQztJQUVJO1FBREEsVUFBSyxHQUFXLEVBQUUsQ0FBQztJQUNKLENBQUM7SUFFaEIsc0NBQWdCLEdBQWhCO1FBQ0ksSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7SUFDcEIsQ0FBQztJQU5RLFdBQVc7UUFEdkIsaUJBQVUsRUFBRTs7T0FDQSxXQUFXLENBT3ZCO0lBQUQsa0JBQUM7Q0FBQSxBQVBELElBT0M7QUFQWSxrQ0FBVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIFNlc3Npb25EYXRhIHtcbiAgICB0b2tlbjogc3RyaW5nID0gXCJcIjtcbiAgICBjb25zdHJ1Y3RvcigpIHt9XG5cbiAgICBjbGVhclNlc3Npb25EYXRhKCkge1xuICAgICAgICB0aGlzLnRva2VuID0gXCJcIjtcbiAgICB9XG59Il19