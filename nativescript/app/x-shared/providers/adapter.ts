import { Injectable } from "@angular/core";
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Rx";
import "rxjs/add/operator/do";
import "rxjs/add/operator/map";

import { MethodMapping } from "./methodMapping";
import { MethodCall } from "@angular/compiler";

@Injectable()
export class Adapter {
    constructor(private http: Http) {}

    postRequest(dataToSend: string, url: string) {
        let headers = new Headers();
        headers.append("Content-Type", "application/json");
      
        return this.http.post(
          MethodMapping.DOMAIN + url,
          JSON.parse(dataToSend),
          { headers: headers }
        ).catch(this.handleErrors);
    }

    getRequest(url:string, token:string) {
        let headers = new Headers();
        headers.append("Content-Type", "application/json");
        headers.append("Authorization", token);
      
        return this.http.get(
          MethodMapping.DOMAIN + url,
          { headers: headers }
        ).catch(this.handleErrors);
    }

    handleErrors(error: Response) {
        console.log(JSON.stringify(error.json()));
        return Observable.throw(error);
      }
}
