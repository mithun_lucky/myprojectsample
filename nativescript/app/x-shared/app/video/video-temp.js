"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Videos = [
    { name: 'Video1', url: 'https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4' },
    { name: 'Video2', url: 'https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4' },
    { name: 'Video3', url: 'https://test.com/' },
    { name: 'Video4', url: 'https://test.com/' },
    { name: 'Video5', url: 'https://test.com/' }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlkZW8tdGVtcC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZGVvLXRlbXAudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFFYSxRQUFBLE1BQU0sR0FBWTtJQUMzQixFQUFDLElBQUksRUFBRSxRQUFRLEVBQUUsR0FBRyxFQUFFLG9EQUFvRCxFQUFDO0lBQzNFLEVBQUMsSUFBSSxFQUFFLFFBQVEsRUFBRSxHQUFHLEVBQUUsb0RBQW9ELEVBQUM7SUFDM0UsRUFBQyxJQUFJLEVBQUUsUUFBUSxFQUFFLEdBQUcsRUFBRSxtQkFBbUIsRUFBQztJQUMxQyxFQUFDLElBQUksRUFBRSxRQUFRLEVBQUUsR0FBRyxFQUFFLG1CQUFtQixFQUFDO0lBQzFDLEVBQUMsSUFBSSxFQUFFLFFBQVEsRUFBRSxHQUFHLEVBQUUsbUJBQW1CLEVBQUM7Q0FDN0MsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFZpZGVvIH0gZnJvbSAnLi4vdmlkZW8vdmlkZW8tbW9kZWwnO1xuXG5leHBvcnQgY29uc3QgVmlkZW9zOiBWaWRlb1tdID0gW1xuICAgIHtuYW1lOiAnVmlkZW8xJywgdXJsOiAnaHR0cHM6Ly9jbGlwcy52b3J3YWVydHMtZ21iaC5kZS9iaWdfYnVja19idW5ueS5tcDQnfSxcbiAgICB7bmFtZTogJ1ZpZGVvMicsIHVybDogJ2h0dHBzOi8vY2xpcHMudm9yd2FlcnRzLWdtYmguZGUvYmlnX2J1Y2tfYnVubnkubXA0J30sXG4gICAge25hbWU6ICdWaWRlbzMnLCB1cmw6ICdodHRwczovL3Rlc3QuY29tLyd9LFxuICAgIHtuYW1lOiAnVmlkZW80JywgdXJsOiAnaHR0cHM6Ly90ZXN0LmNvbS8nfSxcbiAgICB7bmFtZTogJ1ZpZGVvNScsIHVybDogJ2h0dHBzOi8vdGVzdC5jb20vJ31cbl07Il19