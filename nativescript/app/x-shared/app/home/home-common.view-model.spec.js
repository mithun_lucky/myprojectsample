"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var home_common_view_model_1 = require("./home-common.view-model");
describe('HomeCommonViewModel', function () {
    var homeCommonVM;
    beforeEach(function () {
        homeCommonVM = new home_common_view_model_1.HomeCommonViewModel();
    });
    it('should have initial counter value greater than 0', function () {
        expect(homeCommonVM.counter).toBeGreaterThan(0);
    });
    it('::onTap should decreament count by 1', function () {
        var initialCount = homeCommonVM.counter;
        homeCommonVM.onTap();
        expect(homeCommonVM.counter).toEqual(initialCount - 1);
    });
    it('should have tap count left in message when count greater than 0', function () {
        expect(homeCommonVM.message).toContain('taps left');
    });
    it('should have proper message count is 0', function () {
        homeCommonVM.counter = 0;
        expect(homeCommonVM.message).toContain('Hoorraaay! \nYou are ready to start building!');
    });
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9tZS1jb21tb24udmlldy1tb2RlbC5zcGVjLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiaG9tZS1jb21tb24udmlldy1tb2RlbC5zcGVjLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsbUVBQStEO0FBRS9ELFFBQVEsQ0FBQyxxQkFBcUIsRUFBRTtJQUU5QixJQUFJLFlBQWlDLENBQUM7SUFFdEMsVUFBVSxDQUFDO1FBQ1QsWUFBWSxHQUFHLElBQUksNENBQW1CLEVBQUUsQ0FBQztJQUMzQyxDQUFDLENBQUMsQ0FBQztJQUVILEVBQUUsQ0FBQyxrREFBa0QsRUFBRTtRQUNyRCxNQUFNLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNsRCxDQUFDLENBQUMsQ0FBQztJQUVILEVBQUUsQ0FBQyxzQ0FBc0MsRUFBRTtRQUN6QyxJQUFJLFlBQVksR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDO1FBQ3hDLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNyQixNQUFNLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxZQUFZLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFDekQsQ0FBQyxDQUFDLENBQUM7SUFFSCxFQUFFLENBQUMsaUVBQWlFLEVBQUU7UUFDcEUsTUFBTSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDdEQsQ0FBQyxDQUFDLENBQUM7SUFFSCxFQUFFLENBQUMsdUNBQXVDLEVBQUU7UUFDMUMsWUFBWSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUM7UUFDekIsTUFBTSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsQ0FBQyxTQUFTLENBQUMsK0NBQStDLENBQUMsQ0FBQztJQUMxRixDQUFDLENBQUMsQ0FBQztBQUNMLENBQUMsQ0FBQyxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSG9tZUNvbW1vblZpZXdNb2RlbCB9IGZyb20gJy4vaG9tZS1jb21tb24udmlldy1tb2RlbCc7XG5cbmRlc2NyaWJlKCdIb21lQ29tbW9uVmlld01vZGVsJywgKCkgPT4ge1xuXG4gIGxldCBob21lQ29tbW9uVk06IEhvbWVDb21tb25WaWV3TW9kZWw7XG5cbiAgYmVmb3JlRWFjaCgoKSA9PiB7XG4gICAgaG9tZUNvbW1vblZNID0gbmV3IEhvbWVDb21tb25WaWV3TW9kZWwoKTtcbiAgfSk7XG5cbiAgaXQoJ3Nob3VsZCBoYXZlIGluaXRpYWwgY291bnRlciB2YWx1ZSBncmVhdGVyIHRoYW4gMCcsICgpID0+IHtcbiAgICBleHBlY3QoaG9tZUNvbW1vblZNLmNvdW50ZXIpLnRvQmVHcmVhdGVyVGhhbigwKTtcbiAgfSk7XG5cbiAgaXQoJzo6b25UYXAgc2hvdWxkIGRlY3JlYW1lbnQgY291bnQgYnkgMScsICgpID0+IHtcbiAgICBsZXQgaW5pdGlhbENvdW50ID0gaG9tZUNvbW1vblZNLmNvdW50ZXI7XG4gICAgaG9tZUNvbW1vblZNLm9uVGFwKCk7XG4gICAgZXhwZWN0KGhvbWVDb21tb25WTS5jb3VudGVyKS50b0VxdWFsKGluaXRpYWxDb3VudCAtIDEpO1xuICB9KTtcblxuICBpdCgnc2hvdWxkIGhhdmUgdGFwIGNvdW50IGxlZnQgaW4gbWVzc2FnZSB3aGVuIGNvdW50IGdyZWF0ZXIgdGhhbiAwJywgKCkgPT4ge1xuICAgIGV4cGVjdChob21lQ29tbW9uVk0ubWVzc2FnZSkudG9Db250YWluKCd0YXBzIGxlZnQnKTtcbiAgfSk7XG5cbiAgaXQoJ3Nob3VsZCBoYXZlIHByb3BlciBtZXNzYWdlIGNvdW50IGlzIDAnLCAoKSA9PiB7XG4gICAgaG9tZUNvbW1vblZNLmNvdW50ZXIgPSAwO1xuICAgIGV4cGVjdChob21lQ29tbW9uVk0ubWVzc2FnZSkudG9Db250YWluKCdIb29ycmFhYXkhIFxcbllvdSBhcmUgcmVhZHkgdG8gc3RhcnQgYnVpbGRpbmchJyk7XG4gIH0pO1xufSk7XG4iXX0=