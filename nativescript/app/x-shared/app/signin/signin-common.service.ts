import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Rx";

import "rxjs/add/operator/do";
import "rxjs/add/operator/map";

import { User } from "./signin-common.view-model";
import { Adapter } from "../../providers/adapter";
import { MethodMapping } from "../../providers/methodMapping";
import { SessionData } from "../../providers/sessionData";

@Injectable()
export class UserService {
  constructor(private adapter: Adapter, 
    private sessionData: SessionData) {}

  register(user: User) {
    //call adapter.ts to register user
  }

  login(user: User) {
    var data = JSON.stringify({
      username: user.uname,
      password: user.password
    })

    return this.adapter.postRequest(data, MethodMapping.LOGIN)
    .map(response => {
        var tokenValue = response.headers.get("Authorization");
        this.sessionData.token = tokenValue;
    })
  }

  getUserData() {
    return this.adapter.getRequest(MethodMapping.USERS, this.sessionData.token)
    .map(response => {
      //save response in session 
  })
  }
}